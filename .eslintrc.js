module.exports = {
    rules: {
        "@typescript-eslint/no-var-requires": 0,
    },
    parser: "vue-eslint-parser",
    parserOptions: {
        parser: "babel-eslint",
        ecmaVersion: 6,
    },
};
