export interface bgDataInterface {
    path: string;
    show: boolean;
}

export interface urlInterface {
    id: string;
    name: string;
    url: string;
}

export interface dataApiInterface {
    backgroundImgList: bgDataInterface[];
    history: urlInterface[];
    hot: urlInterface[];
}
