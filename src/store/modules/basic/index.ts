/*
 * 使用webpakc读取启导入所有 vuex 模块。
 */

const files = require.context("./modules", false, /\.ts$/);
const modules: any = {};

files.keys().forEach((key: string) => {
    modules[key.replace(/(\.\/|\.ts)/g, "")] = files(key).default;
});

export default {
    namespaced: true,
    modules,
};
