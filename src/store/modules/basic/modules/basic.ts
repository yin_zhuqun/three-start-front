import util from "../../../../utils/data/index";
import router from "@/router";
import store from "@/store";
import { basicInterface } from "@/interface/store/basic.interface";
import { dataApiInterface, urlInterface } from "@/api/data.interface";
import common from "@/components/utils/common";

export default {
    namespaced: true,
    state: {
        info: {
            backgroundImgList: [],
            history: [],
            hot: [],
        },
    },
    getters: {
        getBg(state: basicInterface) {
            return common.filterTruthy(
                state.info.backgroundImgList,
                "show",
                true
            );
        },
        getHistory(state: basicInterface) {
            return state.info.history;
        },
        getHot(state: basicInterface) {
            return state.info.hot;
        },
        getBasicInfo(state: basicInterface) {
            return state.info;
        },
    },
    mutations: {
        saveBasicInfo(
            state: basicInterface,
            { backgroundImgList, history, hot }: dataApiInterface
        ) {
            state.info.backgroundImgList = backgroundImgList;
            state.info.history = history;
            state.info.hot = hot;
        },
        saveHistoryInfo(state: basicInterface, value: urlInterface[]) {
            state.info.history = value;
        },
    },
    actions: {
        /**
         * 保存信息
         * @param {Object} context vuex
         * @param {Object} info 保存信息
         * @returns {Object} info信息
         */
        saveBasicInfo(context: any, info: dataApiInterface) {
            return new Promise(async (resolve) => {
                context.commit("saveBasicInfo", info);
                const setted = await context.dispatch("setInfo", info);
                resolve(setted);
            });
        },
        // --------- 为执行搜索保存单个历史记录而创建，后因采用全部更新而废除，本段代码未使用 start ---------
        /**
         * 保存单个搜索历史记录
         * @param {Object} context vuex
         * @param {Object} info 保存信息
         * @returns {Object} info信息
         */
        // saveHistoryInfo(context: any, historyInfo: urlInterface) {
        //     return new Promise(async (resolve) => {
        //         const newHistoryInfo = [
        //             historyInfo,
        //             ...context.getters["getHistory"].map((el: urlInterface) => {
        //                 return {
        //                     id: el.id,
        //                     name: el.name,
        //                     url: el.url,
        //                 };
        //             }),
        //         ];
        //         context.commit("saveHistoryInfo", newHistoryInfo);

        //         const newBasicInfo = context.getters["getBasicInfo"];
        //         await context.dispatch("setInfo", newBasicInfo);

        //         resolve(true);
        //     });
        // },
        // --------- 为执行搜索保存单个历史记录而创建，后因采用全部更新而废除，本段代码未使用 end ---------
        /**
         * 更新全部历史记录
         * @param context
         * @param historyInfo
         */
        updateHistoryInfo(context: any, historyInfo: urlInterface[]) {
            return new Promise(async (resolve) => {
                context.commit("saveHistoryInfo", historyInfo);
                await context.dispatch("setInfo", context.getters.getBasicInfo);

                resolve(true);
            });
        },
        /**
         * 设置基本信息
         * @param {Object} state vuex state
         * @param {Object} dispatch vuex dispatch
         * @param {*} info info
         */
        setInfo(context: any, info: dataApiInterface) {
            return new Promise(async (resolve) => {
                // 持久化
                await context.dispatch(
                    "basic/db/set",
                    {
                        keyName: "33Start",
                        path: "info",
                        value: info,
                        user: true,
                    },
                    { root: true }
                );
                // end
                resolve(info);
            });
        },
        /**
         * 从数据库取用户数据
         * @param {Object} context vuex
         */
        getInfo(context: any) {
            return new Promise(async (resolve) => {
                // 获取本地信息
                const basicInfo: dataApiInterface = await context.dispatch(
                    "basic/db/get",
                    {
                        keyName: "33Start",
                        path: "info",
                        defaultValue: {},
                        user: true,
                    },
                    { root: true }
                );
                context.commit("saveBasicInfo", basicInfo);

                resolve(basicInfo);
            });
        },
        /**
         * @description 持久化数据加载一系列的设置
         * @param {Object} state vuex state
         * @param {Object} dispatch vuex dispatch
         */
        load(context: any) {
            return new Promise(async (resolve) => {
                // 加载基本信息
                const basicInfo = await context.dispatch("getInfo");
                resolve(basicInfo);
            });
        },
    },
};
