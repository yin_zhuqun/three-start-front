/**
 * 持久化存储或读取数据
 * */
import util from "@/utils/data";
import router from "@/router";
import { cloneDeep } from "lodash";

/**
 * @description 检查路径在lowdb中是否存在
 * @param {Object} keyName 数据名称
 * @param {Object} path 路径
 * @param {Object} user 区分用户
 * @param {Object} validator 数据校验钩子 返回 true 表示验证通过
 * @param {Object} defaultValue 初始化默认值
 * @returns {String} 可以直接使用的路径
 */
function pathInit({
    keyName = "33Start",
    path = "",
    user = true,
    validator = (val: any) => true,
    defaultValue = "",
}) {
    const curPath = `${keyName}${path ? `.${path}` : ""}`;

    const value = util.db.get(curPath).value();

    if (!(value !== undefined && validator(value))) {
        util.db.set(curPath, defaultValue).write();
    }

    return curPath;
}

export { pathInit };

export default {
    namespaced: true,
    actions: {
        /**
         * @description 将数据存储到指定位置 | 路径不存在会自动初始化
         * @param context context
         * @param {Object} keyName 数据库名称
         * @param {Object} path 存储路径
         * @param {Object} value 需要存储的值
         * @param {Object} user  是否区分用户
         */
        set(
            context: any,
            { keyName = "33Start", path = "", value = "", user = false }
        ) {
            util.db
                .set(
                    pathInit({
                        keyName,
                        path,
                        user,
                    }),
                    value
                )
                .write();
        },
        /**
         * @description 获取数据
         * @param context context
         * @param {Object} keyName  数据库名称
         * @param {Object} path  存储路径
         * @param {Object} defaultValue } 取值失败的默认值
         * @param {Object} user  是否区分用户
         */
        get(
            context: any,
            { keyName = "33Start", path = "", defaultValue = "", user = false }
        ) {
            return new Promise((resolve) => {
                resolve(
                    cloneDeep(
                        util.db
                            .get(
                                pathInit({
                                    keyName,
                                    path,
                                    user,
                                    defaultValue,
                                })
                            )
                            .value()
                    )
                );
            });
        },
        //  /**
        //   * @description 获取存储数据库对象
        //   * @param {Object} context context
        //   * @param {Object} user {Boolean} 是否区分用户
        //   */
        //  database (context, {
        // 	 user = false
        //  } = {}) {
        // 	 return new Promise(resolve => {
        // 		 resolve(util.db.get(pathInit({
        // 			 dbName: 'database',
        // 			 path: '',
        // 			 user,
        // 			 defaultValue: {}
        // 		 })))
        // 	 })
        //  },
        /**
         * @description 清空存储数据库对象
         * @param {Object} context context
         * @param {Object} user {Boolean} 是否区分用户
         */
        sysClear(context: any, { user = false } = {}) {
            return new Promise((resolve) => {
                resolve(
                    util.db.get(
                        pathInit({
                            keyName: "sys",
                            path: "",
                            user,
                            validator: () => false,
                            defaultValue: "",
                        })
                    )
                );
            });
        },
    },
};
