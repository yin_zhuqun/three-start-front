import { createStore } from "vuex";

import basic from "./modules/basic/index";

export default createStore({
    modules: {
        basic,
    },
});
