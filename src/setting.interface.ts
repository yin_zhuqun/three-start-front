export interface SettingInterface {
    title: string;
    apiBaseTime: number;
    cookiesExpires: number;
    cookieSign: string;
}
