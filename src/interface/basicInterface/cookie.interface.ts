export interface CookieInterface {
    set?: any;
    get?: any;
    getAll?: any;
    remove?: any;
}
export interface CookieSetInterface {
    expires: number;
}
