interface basicLayout {
    readonly sideWidth?: string;
    readonly headerHeight?: string;
    readonly showSider?: string;
}
export interface themeType {
    readonly name: string;
    readonly color: string;
    config?: boolean;
}

export interface SettingInterface {
    readonly title: string;
    readonly layout: basicLayout;
    readonly apiBaseURL: string;
    readonly apiBaseTime: number;
    readonly cookiesExpires: number;
    readonly cookieSign: string;
    readonly theme: themeType[];
    readonly defaultMenu?: string;
    readonly showSider?: boolean;
}

export interface configInterface {
    readonly baseApiUrl: string;
    readonly mockApiUrl?: string;
}
