import Cookies from "js-cookie";
import {
    CookieInterface,
    CookieSetInterface,
} from "../../interface/basicInterface/cookie.interface";
import Setting from "@/setting";

const cookies: CookieInterface = new Object();
const defaultName = "default";

/**
 * @description 存储 cookie 值
 * @param {String} name cookie name
 * @param {String} value cookie value
 * @param {Object} cookieSetting cookie setting
 */
cookies.set = (name = defaultName, value = "", cookieSetting = {}): void => {
    const setConfig: CookieSetInterface = {
        expires: Setting.cookiesExpires,
    };
    Object.assign(setConfig, cookieSetting);
    Cookies.set(`${Setting.cookieSign}-${name}`, value, setConfig);
};

/**
 * @description 拿到 cookie 值
 * @param {String} name cookie name
 */
cookies.get = (name = defaultName): string | undefined => {
    return Cookies.get(`${Setting.cookieSign}-${name}`);
};

/**
 * @description 拿到 cookie 全部的值
 */
cookies.getAll = (): any => {
    return Cookies.get();
};

/**
 * @description 删除 cookie
 * @param {String} name cookie name
 */
cookies.remove = (name = defaultName): void => {
    return Cookies.remove(`${Setting.cookieSign}-${name}`);
};

export default cookies;
