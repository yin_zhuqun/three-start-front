import low from "lowdb";
import LocalStorage from "lowdb/adapters/LocalStorage";

import Setting from "@/setting";

const adapter: any = new LocalStorage(`${Setting.cookieSign}-admin`);

const db: any = low(adapter);
db.write();

export default db;
