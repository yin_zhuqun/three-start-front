import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// 引入iconfont
import "./style/font/iconfont.css";

import TcDirectivePlus from "tc-directive-plus";
import "tc-directive-plus/src/tc-directive-plus.css";

const app = createApp(App);
app.use(TcDirectivePlus);

app.use(store)
    .use(router)
    .mount("#app");
