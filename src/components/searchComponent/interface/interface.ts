export interface searchType {
    name: string;
    id: string;
    url: string;
}
