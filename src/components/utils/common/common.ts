import { commonInterface } from "./common.interface";
const common: commonInterface = new Object();

common.filterTruthy = function(arr: any[], name: string, value: any = true) {
    if (!Array.isArray(arr)) return [];

    const handleFilter = (
        arrList: any[],
        stringVal: string,
        filterValue: any
    ) => {
        return arrList.filter((el: any) => {
            return el[stringVal] === filterValue;
        });
    };

    const filterValue = handleFilter(arr, name, value);
    if (filterValue.length === 0) {
        return [];
    } else if (filterValue.length === 1) {
        return filterValue[0];
    } else {
        console.error("筛选异常，存在多个筛选结果");
        return filterValue[0];
    }
};
export default common;
