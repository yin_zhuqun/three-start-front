import { SettingInterface } from "@/setting.interface";

const env: string = process.env.NODE_ENV || "";

const Setting: SettingInterface = {
    // 项目标题
    title: process.env.VUE_APP_TITLE,
    // 请求超时时间
    apiBaseTime: 1000000,
    cookiesExpires: 1,
    // 项目的标识，此标识将作为cookie的前缀存入浏览器的cookie
    cookieSign: "33Start",
};

export default Setting;
