import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/layout/baseLayout/index.vue";

const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        name: "Home",
        component: Home,
        redirect: "",
        children: [
            {
                path: "/dashboard",
                name: "dashboard",
                meta: {
                    title: "首页",
                    auth: false,
                    cache: true,
                },
                component: () => import("@/views/pages/home/index.vue"),
            },
        ],
    },
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;
